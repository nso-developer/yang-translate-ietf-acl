# YANG configuration translation of the IETF ACL model

This package implements configuration translation of the IETF Access Control
Lists (ACLs) device YANG model, as defined in [RFC8519]. The input ACL is
configured according to the IETF ACL model and this package then translates it
into a device proprietary configuration and writes it to the device. The
following device types are supported:
- Cisco IOS XR via native NETCONF / YANG
- Juniper JUNOS via the juniper-junos NED (sort of NETCONF)
- Nokia SROS via the alu-sr CLI NED
- Nokia SRSO via native NETCONF / YANG

With the use of `if-feature` statements, different types of ACLs can be defined.
This implementation supports the `ipv4-acl` and `ipv6-acl` feature.


## Usage

The YANG model is augmented in under `/devices/device/translate`. Configure an
ACL according to the IETF model. It will be translated and written to the device
upon committing the configuration.

Here's an example for a border interface ACL according to the IETF ACL model.
```xml
<devices xmlns="http://tail-f.com/ns/ncs">
  <device>
    <name>DUT</name>
    <translate xmlns="http://github.com/nso-developer/yang-translate-container">
      <acls xmlns="urn:ietf:params:xml:ns:yang:ietf-access-control-list">
        <acl>
          <name>IPV4-INTERNET-BORDER-IN-GigabitEthernet0_0_0_11</name>
          <type>ipv4-acl-type</type>
          <aces>
            <ace>
              <name>allow traffic from our neighbor to us</name>
              <matches>
                <ipv4>
                  <destination-ipv4-network>172.29.10.1/32</destination-ipv4-network>
                  <source-ipv4-network>172.29.10.0/24</source-ipv4-network>
                </ipv4>
              </matches>
              <actions>
                <forwarding>accept</forwarding>
              </actions>
            </ace>
            <ace>
              <name>drop traffic sourced from our IPv4 address space (spoofed)</name>
              <matches>
                <ipv4>
                  <source-ipv4-address-container xmlns="urn:telekom:terastream:acl-extensions">IPV4-OUR-AGGREGATES</source-ipv4-address-container>
                </ipv4>
              </matches>
              <actions>
                <forwarding>drop</forwarding>
              </actions>
            </ace>
            <ace>
              <name>allow everything else</name>
              <actions>
                <forwarding>accept</forwarding>
              </actions>
            </ace>
          </aces>
        </acl>
        <attachment-points>
          <interface>
            <interface-id>GigabitEthernet0/0/0/11</interface-id>
            <ingress>
              <acl-sets>
                <acl-set>
                  <name>IPV4-INTERNET-BORDER-IN-GigabitEthernet0_0_0_11</name>
                </acl-set>
              </acl-sets>
            </ingress>
          </interface>
        </attachment-points>
        <address-container xmlns="urn:telekom:terastream:acl-extensions">
          <address-container-type xmlns:acl="urn:ietf:params:xml:ns:yang:ietf-access-control-list">acl:ipv4-acl-type</address-container-type>
          <address-container-name>IPV4-OUR-AGGREGATES</address-container-name>
          <entry>
            <entry-name>192.168.1.0/24</entry-name>
            <ipv4-prefix>192.168.1.0/24</ipv4-prefix>
          </entry>
          <entry>
            <entry-name>192.168.2.0/24</entry-name>
            <ipv4-prefix>192.168.2.0/24</ipv4-prefix>
          </entry>
        </address-container>
      </acls>
    </translate>
  </device>
</devices>
```

It then gets translated into device proprietary configuration, like here below
for JUNOS, which is pretty much a 1:1 mapping. Do note that the interface-name
is not translated but needs to be given as expected on the platform. The above
input is actually for IOS XR but for brevity, we don't include it multiple times
with the different interface names for each platform.

```xml
<devices xmlns="http://tail-f.com/ns/ncs">
  <device>
    <name>DUT</name>
    <config>
      <configuration xmlns="http://xml.juniper.net/xnm/1.1/xnm">
        <interfaces>
          <interface>
            <name>ge-0/0/11</name>
            <unit>
              <name>0</name>
              <description>Peer with Connection to hub: 276-R2-1-ibgp</description>
              <family>
                <inet>
                  <filter>
                    <input-list>IPV4-INTERNET-BORDER-IN-ge-0_0_11</input-list>
                  </filter>
                </inet>
              </family>
              ...
            </interface>
          </interface>
        </interfaces>
        <policy-options>
          <prefix-list>
            <name>IPV4-OUR-AGGREGATES</name>
            <prefix-list-item>
              <name>192.168.1.0/24</name>
            </prefix-list-item>
            <prefix-list-item>
              <name>192.168.2.0/24</name>
            </prefix-list-item>
          </prefix-list>
        </policy-options>
        <firewall>
          <family>
            <inet>
              <filter>
                <name>IPV4-INTERNET-BORDER-IN-ge-0_0_11</name>
                <term>
                  <name>allow traffic from our neighbor to us</name>
                  <from>
                    <source-address>
                      <name>172.29.10.0/24</name>
                    </source-address>
                    <destination-address>
                      <name>172.29.10.1/32</name>
                    </destination-address>
                  </from>
                  <then>
                    <accept/>
                  </then>
                </term>
                <term>
                  <name>drop traffic sourced from our IPv4 address space (spoofed)</name>
                  <from>
                    <source-prefix-list>
                      <name>IPV4-OUR-AGGREGATES</name>
                    </source-prefix-list>
                  </from>
                  <then>
                    <discard/>
                  </then>
                </term>
                <term>
                  <name>allow everything else</name>
                  <then>
                    <accept/>
                  </then>
                </term>
              </filter>
            </inet>
          </family>
        </firewall>
    </config>
  </device>
</devices>
```

For IOS XR, it looks a little different as IOS XR does not support the
prefix-list concept, so the list of addresses is flattened by the configuration
translation package to become a multiple access list entries in the resulting
ACL. Sequence numbers are also added where necessary.

```xml
<devices xmlns="http://tail-f.com/ns/ncs">
  <device>
    <name>DUT</name>
    <config>
      <interface-configurations xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg">
        <interface-configuration>
          <active>act</active>
          <interface-name>GigabitEthernet0/0/0/11</interface-name>
          <ipv6-packet-filter xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ip-pfilter-cfg">
            <inbound>
              <acl-name-array>IPV6-INTERNET-BORDER-IN-GigabitEthernet0_0_0_11</acl-name-array>
              <is-common-array>false</is-common-array>
            </inbound>
          </ipv6-packet-filter>
          ...
        </interface-configuration>
      </interface-configurations>
      <ipv4-acl-and-prefix-list xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-acl-cfg">
        <accesses>
          <access>
            <access-list-name>IPV4-INTERNET-BORDER-IN-GigabitEthernet0_0_0_11</access-list-name>
            <access-list-entries>
              <access-list-entry>
                <sequence-number>1</sequence-number>
                <grant>permit</grant>
                <protocol-operator>equal</protocol-operator>
                <source-network>
                  <source-address>172.29.10.0</source-address>
                  <source-prefix-length>24</source-prefix-length>
                </source-network>
                <destination-network>
                  <destination-address>172.29.10.1</destination-address>
                  <destination-prefix-length>32</destination-prefix-length>
                </destination-network>
                <sequence-str>1</sequence-str>
              </access-list-entry>
              <access-list-entry>
                <sequence-number>2</sequence-number>
                <grant>deny</grant>
                <protocol-operator>equal</protocol-operator>
                <source-network>
                  <source-address>192.168.1.0</source-address>
                  <source-prefix-length>24</source-prefix-length>
                </source-network>
                <sequence-str>2</sequence-str>
              </access-list-entry>
              <access-list-entry>
                <sequence-number>3</sequence-number>
                <grant>deny</grant>
                <protocol-operator>equal</protocol-operator>
                <source-network>
                  <source-address>192.168.2.0</source-address>
                  <source-prefix-length>24</source-prefix-length>
                </source-network>
                <sequence-str>3</sequence-str>
              </access-list-entry>
              <access-list-entry>
                <sequence-number>4</sequence-number>
                <grant>permit</grant>
                <protocol-operator>equal</protocol-operator>
                <sequence-str>4</sequence-str>
              </access-list-entry>
            </access-list-entries>
          </access>
        </accesses>
      </ipv4-acl-and-prefix-list>
    </config>
  </device>
</devices>
```

And similarly for Nokia SROS:
```xml
<devices xmlns="http://tail-f.com/ns/ncs">
  <device>
    <name>276-R1-5</name>
    <config>
      <configure xmlns="urn:nokia.com:sros:ns:yang:sr:conf">
        <filter>
          <match-list>
            <ip-prefix-list>
              <prefix-list-name>IPV4-OUR-AGGREGATES</prefix-list-name>
              <prefix>
                <ip-prefix>192.168.1.0/24</ip-prefix>
              </prefix>
              <prefix>
                <ip-prefix>192.168.2.0/24</ip-prefix>
              </prefix>
            </ip-prefix-list>
          </match-list>
          <ip-filter>
            <filter-name>2158</filter-name>
            <entry>
              <entry-id>1</entry-id>
              <description>allow traffic from our neighbor to us</description>
              <match>
                <src-ip>
                  <address>192.168.21.0/30</address>
                </src-ip>
                <dst-ip>
                  <address>192.168.21.2/32</address>
                </dst-ip>
              </match>
              <action>
                <accept/>
              </action>
            </entry>
            <entry>
              <entry-id>2</entry-id>
              <description>drop traffic sourced from our IPv4 address space (spoofed)</description>
              <match>
                <src-ip>
                  <ip-prefix-list>IPV4-OUR-AGGREGATES</ip-prefix-list>
                </src-ip>
              </match>
              <action>
                <drop/>
              </action>
            </entry>
            <entry>
              <entry-id>3</entry-id>
              <description>allow everything else</description>
              <action>
                <accept/>
              </action>
            </entry>
          </ip-filter>
        </filter>
        <router>
          <router-name>Base</router-name>
          <interface>
            <interface-name>port-1/1/3</interface-name>
            <admin-state>enable</admin-state>
            <port>1/1/3</port>
            <ingress>
              <filter>
                <ip>2158</ip>
              </filter>
            </ingress>
            ...
          </interface>
      </configure>
    </config>
  </device>
</devices>
```


## Installation

Due to the way config templates are validated with ned-id in NSO, it is
difficult to support generic packages that write directly to devices, like this
package.

NSO performs strict template validation at load time, which means that all NEDs
referenced from the configuration template need to be loaded in NSO. Most Cisco
provided NEDs contain a version number as part of the ned-id, like alu-sr-8.7,
which needs to match exactly between the template and loaded NEDs. Even if you
are only interested in using ACL translation for Cisco IOS XR, this means you
would still need to load the alu-sr NED in order to get past template load
checks.

Newer versions of NSO features a `supported-ned-id` directive, to optionally
use a particular ned-id only if that ned-id is available in NSO. However, it
still does exact ned-id matching and since the version is embedded in the
ned-id, supporting multiple versions would require enumerating all ned-ids in
`package-meta-data.xml` and to repeat the configuration stanzas in the template
wrapped in an if-ned-id condition for every possible ned-id.

This package opts for a custom build-time step that preprocesses the
configuration templates to remove configuration that do not match a loaded
ned-id and rewrite the version component of ned-ids to match the loaded NEDs.

If you are using NSO in Docker, the extra build time step is automatically run
at composition time, that is when this package is included in another
repository.

You can manually run the extra step by running `make compose` but you must
ensure that it is built in an environment where all NEDs that you are going to
use in production are loaded.


## NSO tweaks

To make the package work correctly in Cisco NSO, a small change needs to be
made.

Due to a possible bug in ncsc, the desired features cannot be enabled by
passing a feature flag, but are rather uncommented directly in the YANG
model.

## Terastream extensions

In the YANG module `terastream-acl-extensions`, we define various extensions to
the standard model.

### Address Container

The standard model allows you to specify individual source and destination
networks for each entry in the ACL. If you have muliple networks or addresses
that form a single entity from a security management perspective (i.e. multiple
management subnets), the number of ACL entries grows every time a new network
needs to be added to the group.

Using the address container, you can group multiple networks into a single
entity. This entity can then be referenced in ACL entries as needed, and will
be expanded depending on the support of the platform.

#### Sample configuration

```
admin@ncs> show configuration acls
acl ipv6-acl test {
    access-list-entries {
        ace lo {
            matches {
                ipv6-acl {
                    protocol                           6;
                    destination-port-range {
                        lower-port 22;
                    }
                    destination-ipv6-address-container test;
                }
            }
        }
    }
}
address-container ipv6-acl test {
    entry fala {
        ipv6-prefix ::1/128;
    }
}
```

[RFC8519]: https://datatracker.ietf.org/doc/rfc8519/
