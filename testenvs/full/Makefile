include ../testenv-common.mk

# Do not run the observability stack by default for most local testenvs in this
# repository.
NSO_OBSERVABILITY?=false
include ../common/nso_observability.mk

# TESTID is unique per test invocation
# For example, invoking 'make test' twice in a local testenv will yield
# different TESTID. It is however stable across one test run.
ifndef TESTID
TESTID:=$(PNS)-$(shell date +%s)
endif
export TESTID

SHELL=/bin/bash

# Define the NSO container name once
export NSO_CNT=$(CNT_PREFIX)-nso

NED_REPOS=$(filter ned-%,$(shell ls ../../includes))

# start: start the test environment in a configuration that allows
# Python Remote Debugging. Exposes port 5678 on a random port on localhost.
start:
	docker network inspect $(CNT_PREFIX) >/dev/null 2>&1 || docker network create $(CNT_PREFIX)
	docker run -td --name $(NSO_CNT) --network-alias nso $(DOCKER_NSO_ARGS) -e ADMIN_PASSWORD=NsoDocker1337 $${NSO_EXTRA_ARGS} $(IMAGE_PATH)$(PROJECT_NAME)/testnso:$(DOCKER_TAG)
	$(MAKE) start-netsim
	$(MAKE) nsobs-start
	time docker exec -t $(NSO_CNT) bash -lc 'ncs --wait-started 600'
	$(MAKE) nsobs-prepare nsobs-config-nso nsobs-print-ui-addresses

start-netsim: $(addprefix start-netsim/,$(NED_REPOS))

start-netsim/%: NED_REPO=$*
# We must use the same version of the "netsim" image as we did for our "package"
# image in /includes. We use a shell too expand the variables used in
# /includes/foo and replace string "package" with "netsim" in the tag. The env
# vars used in includes/foo (typically ${PKG_PATH} and ${NSO_VERSION}) must be
# passed to the shell doing the expansion. Although the (make) variables are
# exported in nidvars.mk and nidcommon.mk, they are only made available as
# environment variables to subshells in *recipes*. The $(NETSIM_IMAGE) variable
# defined here needs an explicit export.
start-netsim/%: NETSIM_IMAGE=$(shell awk '{ print "echo", $$0 }' $(PROJECT_DIR)/includes/$(NED_REPO) | NSO_VERSION=$(NSO_VERSION) PKG_PATH=$(PKG_PATH) $(SHELL) | sed -e 's,/package,/netsim,')
start-netsim/%:
	docker run -td --name $(CNT_PREFIX)-dut-$(NED_REPO) --network-alias dut-$(NED_REPO) $(DOCKER_ARGS) $(NETSIM_IMAGE)

add-device: $(addprefix add-device/,$(NED_REPOS))

add-device/%: NED_REPO=$*
add-device/%: add-device.xml
	-@$(MS)$(TSEQ)
	@echo "-- Add device $(NED_REPO) to NSO"
	@echo "   Get the package-meta-data.xml file from the compiled NED (we grab it from the netsim build)"
# The netsim container only has a single version (ned-id) in the packages directory
	docker exec $(CNT_PREFIX)-dut-$(NED_REPO) bash -c "cp /var/opt/ncs/packages/*/package-meta-data.xml /tmp"
	mkdir -p tmp
	docker cp $(CNT_PREFIX)-dut-$(NED_REPO):/tmp/package-meta-data.xml tmp/package-meta-data-$(NED_REPO).xml
	@echo "   Fill in the device-type in add-device.xml by extracting the relevant part from the package-meta-data of the NED"
	xmlstarlet sel -t -c "//*[_:ned-id] | //_:ned/_:netconf | //_:ned/_:cli | //_:ned/_:snmp | //_:ned/_:generic" tmp/package-meta-data-$(NED_REPO).xml | xmlstarlet edit --subnode '_:*[not(_:ned-id)]' --type elem -n ned-id -v dummy | xmlstarlet sel -R -t -c '/' -c "document('add-device.xml')" | xmlstarlet edit -O -N x=http://tail-f.com/ns/ncs-packages -N y=http://tail-f.com/ns/ncs -d "/x:xsl-select/*[x:ned-id]/*[not(self::x:ned-id)]" -m "/x:xsl-select/*[x:ned-id]" "/x:xsl-select/y:devices/y:device/y:device-type" -u "/x:xsl-select/y:devices/y:device/y:name" -v "dut-$(NED_REPO)" -u "/x:xsl-select/y:devices/y:device/y:address" -v "dut-$(NED_REPO)" $(STRIP_NED) | tail -n +2 | sed '$$d' | cut -c 3- > tmp/add-device-$(NED_REPO).xml
	$(MAKE) loadconf FILE=tmp/add-device-$(NED_REPO).xml
	$(MAKE) runcmdJ CMD="show devices brief"
	$(MAKE) runcmdJ CMD="request devices device dut-$(NED_REPO) ssh fetch-host-keys"
	$(MAKE) runcmdJ CMD="request devices device dut-$(NED_REPO) sync-from"
	-@$(ME)$(TSEQ)

create-acl: $(addprefix create-acl/,$(NED_REPOS))

create-acl/%: NED_REPO=$*
create-acl/%: test-acl.xml
	-@$(MS)$(TSEQ)
	xmlstarlet edit -O -N ncs=http://tail-f.com/ns/ncs -N y=http://github.com/nso-developer/yang-translate-container -N acl=urn:ietf:params:xml:ns:yang:ietf-access-control-list -N aclx=urn:telekom:terastream:acl-extensions \
		--update "/ncs:devices/ncs:device/ncs:name" -v "dut-$(NED_REPO)" \
		test-acl.xml > tmp/test-acl-$(NED_REPO).xml
	$(MAKE) loadconf FILE=tmp/test-acl-$(NED_REPO).xml

save-config: $(addprefix save-config/,$(NED_REPOS))

save-config/%: NED_REPO=$*
save-config/%:
	@echo "-- Saving /devices/device/config to output/config-$(NED_REPO).xml"
	$(MAKE) saveconfxml FILE=output/config-$(NED_REPO).xml CONFPATH="devices device dut-$(NED_REPO) config"

compare-config: $(addprefix compare-config/,$(NED_REPOS))

compare-config/%: NED_REPO=$*
compare-config/%:
	@echo "-- Comparing output/config-$(NED_REPO).xml with expected/config-$(NED_REPO).xml"
	@echo "Stripping out hostname and aaa related config before comparing"
	egrep -v '<host-name>|<message>' expected/config-$(NED_REPO).xml | xmlstarlet ed -N tf="http://tail-f.com/ns/aaa/1.1" -N nc="urn:ietf:params:xml:ns:yang:ietf-netconf-acm" -d '//tf:aaa' -d '//nc:nacm' > /tmp/config-$(NED_REPO).xml.expected
	egrep -v '<host-name>|<message>' output/config-$(NED_REPO).xml | xmlstarlet ed -N tf="http://tail-f.com/ns/aaa/1.1" -N nc="urn:ietf:params:xml:ns:yang:ietf-netconf-acm" -d '//tf:aaa' -d '//nc:nacm' > /tmp/config-$(NED_REPO).xml.output
	diff -u /tmp/config-$(NED_REPO).xml.expected /tmp/config-$(NED_REPO).xml.output

update-config: $(addprefix update-config/,$(NED_REPOS))

update-config/%: NED_REPO=$*
update-config/%:
	@echo "-- Updating expected/config-$(NED_REPO).xml with output/config-$(NED_REPO).xml"
	cp output/config-$(NED_REPO).xml expected/

test:
	$(MAKE) add-device
	$(MAKE) create-acl
	$(MAKE) save-config
	$(MAKE) compare-config

.PHONY: start add-device create-acl save-config compare-config update-config

