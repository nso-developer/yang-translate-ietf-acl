import ipaddress
import unittest
from ietf_acl.acl_yanglate_service import * # pylint: disable = W0401, W0614


class TestAccessListFilters(unittest.TestCase):
    def setUp(self):
        self.ac = {'ac-v6': AddressContainer(Type.ipv6, "ac-v6",
                                             prefixes=[ipaddress.ip_interface('::1/128')])}
        self.acl = [AccessListEntry("some entry", Type.ipv6, Action.accept, Protocol.tcp,
                                    source_address_container='ac-v6')]

    def test_address_container_expand(self):
        filters = (expand_address_containers,)
        for f in filters:
            self.acl = f(self.acl, self.ac) # type: ignore
        transformed = [AccessListEntry("some entry", Type.ipv6, Action.accept, Protocol.tcp,
                                       source_prefix=ipaddress.ip_interface('::1/128'))]
        transformed[0].comments = ['Expanded from ac-v6']
        self.assertListEqual(transformed, list(self.acl))

    def test_address_container_expand_and_add_entry_id(self):
        filters = (expand_address_containers, add_entry_id)
        for f in filters:
            self.acl = f(self.acl, self.ac) # type: ignore
        transformed = [AccessListEntry("some entry", Type.ipv6, Action.accept, Protocol.tcp,
                                       source_prefix=ipaddress.ip_interface('::1/128'))]
        transformed[0].comments = ['Expanded from ac-v6']
        transformed[0].entry_id = 1
        self.assertListEqual(transformed, list(self.acl))

    def test_template_vars_acl(self):
        filters = (generate_template_vars_acl,)
        for f in filters:
            self.acl = f(self.acl) # type: ignore
        transformed = [('acl-yanglate-access-list-entry',
                        [('ACTION', "'accept'"), ('COMMENTS', "''"), ('CONTAINS_MATCH', "'true'"),
                         ('DESTINATION_ADDRESS_CONTAINER', "''"), ('DESTINATION_NETMASK', "''"),
                         ('DESTINATION_NETWORK', "''"), ('DESTINATION_PORT', "''"),
                         ('DESTINATION_PORT_NAME', "''"),
                         ('DESTINATION_PORT_UM_NAME', "''"),
                         ('DESTINATION_PREFIX', "''"), ('DESTINATION_PREFIX_LENGTH', "''"),
                         ('DESTINATION_WILDCARD_PREFIX_LENGTH', "''"), ('ENTRY_ID', "''"),
                         ('NAME', "'some entry'"), ('PROTOCOL', "'tcp'"),
                         ('PROTOCOL_NUMBER', "'6'"), ('PROTOCOL_TYPE', "'tcp'"), ('SOURCE_ADDRESS_CONTAINER', "'ac-v6'"),
                         ('SOURCE_NETMASK', "''"), ('SOURCE_NETWORK', "''"), ('SOURCE_PORT', "''"),
                         ('SOURCE_PORT_NAME', "''"),
                         ('SOURCE_PORT_UM_NAME', "''"),
                         ('SOURCE_PREFIX', "''"), ('SOURCE_PREFIX_LENGTH', "''"),
                         ('SOURCE_WILDCARD_PREFIX_LENGTH', "''"), ('TTL', "''"),
                         ('TYPE', "'ipv6'")])]
        self.assertListEqual(transformed, list(self.acl))

    def test_template_vars_address_container(self):
        filters = (generate_template_vars_address_container,)
        for f in filters:
            self.acl = f(self.ac) # type: ignore
        transformed = [('acl-yanglate-address-container-entry',
                        [('NAME', "'ac-v6'"), ('TYPE', "'ipv6'"), ('PREFIX', "'::1/128'"),
                         ('NETWORK', "'::1'"),
                         ('NETMASK', "'ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff'"),
                         ('PREFIX_LENGTH', "'128'")])]
        self.assertListEqual(transformed, list(self.acl))
