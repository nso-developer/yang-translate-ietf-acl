"""The ACL yanglate services allow us to configure ACLs on devices using the standard IETF ACL model

For a detailed explanation / visualisation of the model, see RFC and/or pyang.

The module contains two services implementations, rooted in the /devices/device{X}/translate container:
- /acls/acl
- /acls/address-container

The data in these standard models is transformed to device-specific configuration with the two
services.

Since different devices have different requirements for the ACL data and different limitations,
the services first transform the data using a pipeline with a series of filters (actual use depends
on the platform). After the data is prepared, another pipeline of generators prepares the template
variables and applies the appropriate templates.

For example, platform Cisco XRv does not support address containers (=object groups on IOS).
We execute the filters: add_entry_id, expand_address_containers,
followed by the generators: generate_template_vars_acl

These filters make sure that the ACL entries using an address container are expanded to individual
entries on the address container. The generator then applies the ACL entry template for all expanded
items that weren't specified explicitly in the input data.

We use two classes to read the data from CDB and input to pipelines: AccessListEntry and
AddressContainer.
The public attributes and properties on these classes store data that will be useful for
generating the configuration.

The filters (implemented as generator functions in this module) must accept an Iterable of
AccessListEntry objects and a dict of AddressContainer objects, and must return an Iterable of
new/transformed AccessListEntry objects:

    def foo_filter(entries: Iterable[AccessListEntry],
                   address_containers: Dict[str, AddressContainer]) -> Iterable[AccessListEntry]:
        # take an Iterable of AccessListEntry objects and a dict of AddressContainer objects
        # and generate new/transformed AccessListEntry objects

        # yield from entries

The template generators must accept the same input parameters as filters, except their output is
a tuple of template_name and template_variables:

    def generate_template_vars_acl(entries: Iterable[AccessListEntry],
                                   address_containers: Dict[str, AddressContainer]):
        yield 'template', tvars

In the current implementation, the generators prepare the template variables by using all
public attributes and properties of the DTOs.

"""

import copy
import enum
import ipaddress
from collections import Counter
from typing import Dict, Iterable, Union, Tuple, List, Optional, Callable

import ncs
from ncs.application import Service
from . import TemplateName


class Type(enum.Enum):
    ipv4 = 4
    ipv6 = 6

    @staticmethod
    def from_yang(yang_type):
        type_lookup = {'acl:ipv4-acl-type': Type.ipv4,
                       'acl:ipv6-acl-type': Type.ipv6}

        if yang_type not in type_lookup:
            raise ValueError(f'ACL type {yang_type} unsupported')
        else:
            return type_lookup[yang_type]


class Action(enum.Enum):
    accept = 0
    drop = 1
    reject = 2

    @staticmethod
    def from_identity(action):
        # strip the 'acl:' prefix
        return Action[action[4:]]


class Protocol(enum.Enum):
    icmp = 1
    ip = 4
    ipv4 = 4
    tcp = 6
    udp = 17
    ipv6 = 41
    icmpv6 = 58


class Dumpable(object):
    def __repr__(self):
        # dump all attributes and values, except if names start with '_' or are functions
        props = ', '.join(f'{x}={self.__getattribute__(x)}'
                          for x in dir(self) if not (x.startswith('_') or callable(self.__getattribute__(x))))
        return f'{self.__class__.__name__}: {props}'


class AddressContainer(Dumpable):
    def __init__(self, ac_type: Type, name: str, prefixes=None) -> None:
        self.type = ac_type
        self.name = name
        self.prefixes = prefixes

    @staticmethod
    def from_cdb(ac):
        address_container = AddressContainer(Type.from_yang(ac.address_container_type), ac.address_container_name)
        prefix_leaf = f'{address_container.type.name}_prefix'
        address_container.prefixes = [ipaddress.ip_interface(getattr(entry, prefix_leaf)) for entry in ac.entry]
        return address_container


class AccessListEntry(Dumpable):
    """An DTO for a single access list entry.

    Created from the ACL service create() method, using AccessListEntry.from_cdb.
    Contains some additional calculated properties needed when creating platform-specific configuration.

    Setting source_prefix (1.2.3.0/24) also generates read-only attributes:
    - source_network: 1.2.3.0
    - source_prefix_length: 24
    - source_netmask: 255.255.255.0

    The same mapping exists for destination_prefix."""

    _source_prefix = None
    source_netmask = None
    source_network = None
    source_wildcard_prefix_length = None
    source_prefix_length = None
    _destination_prefix = None
    destination_netmask = None
    destination_network = None
    destination_wildcard_prefix_length = None
    destination_prefix_length = None

    def __init__(self, name: str, acl_type: Type, action: Action, protocol: Protocol=None,
                 source_prefix: ipaddress._BaseNetwork=None, source_address_container=None, source_port=None,
                 destination_prefix: ipaddress._BaseNetwork=None, destination_address_container=None, destination_port=None) -> None:
        self.name = name
        self.type = acl_type
        self.action = action
        self.protocol = protocol
        self.source_prefix = source_prefix
        self.source_address_container = source_address_container
        self.source_port = source_port
        self.destination_prefix = destination_prefix
        self.destination_address_container = destination_address_container
        self.destination_port = destination_port
        self.ttl = None

        # metadata fields
        self.comments: List[str] = []
        self.entry_id: Optional[int] = None

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @property
    def source_prefix(self):
        try:
            return self._source_prefix
        except AttributeError:
            pass

    @source_prefix.setter
    def source_prefix(self, value):
        if value:
            self._source_prefix = value
            self.source_network = value.network.network_address
            self.source_prefix_length = value.network.prefixlen
            self.source_wildcard_prefix_length = self.wildcard_prefix(self._source_prefix)
            self.source_netmask = value.network.netmask
        else:
            self.source_network = None
            self.source_prefix_length = None
            self.source_wildcard_prefix_length = None
            self.source_netmask = None

    @property
    def destination_prefix(self):
        try:
            return self._destination_prefix
        except AttributeError:
            pass

    @destination_prefix.setter
    def destination_prefix(self, value):
        if value:
            self._destination_prefix = value
            self.destination_network = value.network.network_address
            self.destination_prefix_length = value.network.prefixlen
            self.destination_wildcard_prefix_length = self.wildcard_prefix(self._destination_prefix)
            self.destination_netmask = value.network.netmask
        else:
            self.destination_network = None
            self.destination_prefix_length = None
            self.destination_wildcard_prefix_length = None
            self.destination_netmask = None

    @property
    def protocol_number(self) -> int:
        """Translate the protocol attr value to the (numeric) value"""
        if self.protocol:
            return self.protocol.value
        else:
            return -1

    @property
    def protocol_type(self) -> str:
        """Translate the protocol attr value to the (string) name"""
        if self.protocol:
            return self.protocol.name
        else:
            return ''

    @property
    def destination_port_name(self) -> str:
        return port_to_string(self.destination_port)

    @property
    def source_port_name(self) -> str:
        return port_to_string(self.source_port)

    # UM modules translate port to upper case
    @property
    def destination_port_um_name(self) -> str:
        return port_to_string(self.destination_port, True)

    @property
    def source_port_um_name(self) -> str:
        return port_to_string(self.source_port, True)

    @property
    def contains_match(self) -> bool:
        """Does the ACE contain any match rules?"""
        return any((self.source_prefix, self.source_address_container, self.source_port,
                    self.destination_prefix, self.destination_address_container, self.destination_port,
                    self.protocol_type))


    @staticmethod
    def wildcard_prefix(prefix: Union[ipaddress.IPv4Interface, ipaddress.IPv6Interface]):
        """Convert prefixlen to "wildcard" prefixlen with special behavior for host networks"""
        if prefix.version == 6 and prefix.network.prefixlen == 128:
            return 0
        else:
            return prefix.network.prefixlen

    @staticmethod
    def from_cdb(acl_type: Type, ace):
        entry = AccessListEntry(ace.name, acl_type, Action.from_identity(ace.actions.forwarding))

        # check the ace/matches/l3 choice node. will contain the case name: ipv4 or ipv6
        if ace.matches.l3:
            l3 = str(ace.matches.l3)
            l3_container = getattr(ace.matches, l3)

            # try using the protocol leaf (uint8). if nothing is specified, then we're not interested
            # in the next encapsulated protocol
            if l3_container.protocol:
                entry.protocol = Protocol(l3_container.protocol)

            for direction in ('source', 'destination'):
                choice_name = f'{direction}_network'
                network_choice = getattr(l3_container, choice_name)
                if network_choice == f'{direction}-{l3}-network':
                    network = getattr(l3_container, f'{direction}_{l3}_network')
                    # convert network/prefixlen to IPv4Network/IPv4Network object
                    ip_network = ipaddress.ip_interface(network)
                    attr_name = f'{direction}_prefix'
                    setattr(entry, attr_name, ip_network)
                elif network_choice == f'{direction}-{l3}-address-container':
                    address_container = getattr(l3_container, f'{direction}_{l3}_address_container')
                    prop_name = f'{direction}_address_container'
                    setattr(entry, prop_name, address_container)
            entry.ttl = l3_container.ttl

        # l4 choice structure (example for udp, source port)
        # module: ietf-access-control-list
        #     +--rw acls
        #        +--rw acl* [name]
        #           +--rw aces
        #              +--rw ace* [name]
        #                 +--rw matches
        #                    +--rw (l4)?
        #                       +--:(udp)
        #                          +--rw udp
        #                             +--rw length?             uint16
        #                             +--rw source-port
        #                                +--rw (source-port)?
        #                                   +--:(range-or-operator)
        #                                      +--rw (port-range-or-operator)?
        #                                         +--:(range)
        #                                         |  +--rw lower-port    inet:port-number
        #                                         |  +--rw upper-port    inet:port-number
        #                                         +--:(operator)
        #                                            +--rw operator?     operator
        #                                            +--rw port          inet:port-number

        # check the ace/matches/l4 choice node. will contain the case name: tcp, udp or icmp
        if ace.matches.l4:
            l4 = str(ace.matches.l4)
            # overrides the previously configured protocol?!
            entry.protocol = Protocol[l4]
            l4_container = getattr(ace.matches, l4)

            for direction in ('source', 'destination'):
                prop_name = f'{direction}_port'
                if getattr(l4_container, prop_name):
                    port_container = getattr(l4_container, prop_name)
                    port_choice = getattr(port_container, prop_name)
                    # the source/destination-port choice node only has a single case: range-or-operator
                    if port_choice == 'range-or-operator':
                        if port_choice.port_range_or_operator and port_choice.port_range_or_operator != 'operator':
                            raise ValueError(f'AccessListEntry {ace.name}: only operator case supported for L4 {direction} port specification')
                        if port_container.operator != 'eq':
                            raise ValueError(f'AccessListEntry {ace.name}: only eq operator supported for L4 {direction} port specification')
                        setattr(entry, prop_name, port_container.port)

        return entry


def add_entry_id(entries: Iterable[AccessListEntry],
                 _address_containers: Dict[str, AddressContainer]) -> Iterable[AccessListEntry]:
    """Filter for adding a incrementing sequence id to each ACL entry"""
    entry_id = 1
    for entry in entries:
        entry.entry_id = entry_id
        entry_id += 1
        yield entry


def _expand_address_container_direction(direction: str, entries: Iterable[AccessListEntry], address_containers: Dict[str, AddressContainer]) -> Iterable[AccessListEntry]:
    for acl_entry in entries:
        ac_name = getattr(acl_entry, f'{direction}_address_container')
        if ac_name:
            for ac_entry in address_containers[ac_name].prefixes:
                new_acl_entry = copy.copy(acl_entry)
                new_acl_entry.comments.append(f'Expanded from {ac_name}')
                setattr(new_acl_entry, f'{direction}_prefix', ac_entry)
                setattr(new_acl_entry, f'{direction}_address_container', None)
                yield new_acl_entry
        else:
            yield acl_entry


def expand_address_containers(entries: Iterable[AccessListEntry], address_containers: Dict[str, AddressContainer]) -> Iterable[AccessListEntry]:
    """Filter for expanding (source|destination)_address_container to individual entries.

    Will create n ACEs for n entries in the address container and unset the address container in
    new ACEs."""
    yield from _expand_address_container_direction('source',
                                                   _expand_address_container_direction(
                                                       'destination',
                                                       entries,
                                                       address_containers),
                                                   address_containers)


def port_to_string(port, um_module=False) -> str:
    result: str = ''
    match(port):
        case 22 if um_module:
            result = 'ssh'
        case 53:
            result = 'dns'
        case 123:
            result = 'ntp'
        case 161:
            result = 'snmp'
        case _:
            result = str(port) if port else ''
    return result.upper() if um_module else result

def generate_template_vars_acl(entries: Iterable[AccessListEntry]) -> Iterable[Tuple[TemplateName, ncs.template.Variables]]:
    """Generator for the ACL entry template"""
    for acl_entry in entries:
        tvars = ncs.template.Variables()
        for x in dir(acl_entry):
            if x.startswith('_') or callable(getattr(acl_entry, x)):
                continue
            prop = getattr(acl_entry, x)
            if isinstance(prop, enum.Enum):
                prop = prop.name
            tvars.add(x.upper(), prop or '')
        yield TemplateName('acl-yanglate-access-list-entry'), tvars


def generate_template_vars_address_container(address_containers: Dict[str, AddressContainer]) -> Iterable[Tuple[TemplateName, ncs.template.Variables]]:
    """Generator for address container entry template"""
    for ac in address_containers.values():
        for prefix in ac.prefixes:
            tvars = ncs.template.Variables()
            tvars.add('NAME', ac.name)
            tvars.add('TYPE', ac.type.name)
            tvars.add('PREFIX', prefix)
            tvars.add('NETWORK', prefix.network.network_address)
            tvars.add('NETMASK', prefix.network.netmask)
            tvars.add('PREFIX_LENGTH', prefix.network.prefixlen)
            yield TemplateName('acl-yanglate-address-container-entry'), tvars


def calculate_sros_filter_id(intf_number) -> int:
    """Calculate ACL filter-id for SROS

    :param intf_number: interface id in format "x/y/z"

    ACLs on SROS are identified by a filter-id which is a 16 bit unsigned
    integer, we need it to be unique per port and direction (in / out).
    The natural key we have is the port, like x/y/z. Since we only have
    16 bits we have to be clever about how we map things.
    direction is 1 bit, x/y/z, x is the slot in the router, current gen
    does 40 slots max, so 6 bits = 64 is enough. Second is the sub-slot,
    I think 3 bits is enough (are there ever more than 8 sub-slots?),
    which leaves 16-(6+3) = 6 bits or 64 interfaces per sub-slot.
    The last remaining bit can be used for direction.

    We'll offset the whole thing with an offset defined by 1000,
    so there is no overlap with manually configured stuff.

    bits: 0123456789abcdef
          ^^^^^^------------ slot, 6 bits, shifted 10
                ^^^--------- sub-slot, 3 bits, shifted 7
                   ^^^^^^--- port on interface, 6 bits, shifted 1
                         ^-- direction, 0 = in, 1 = out (not assigned here)
    """
    pp = [int(x) for x in intf_number.split('/')]
    sros_filter_id = (pp[0] << 10) + (pp[1] << 7) + (pp[2] << 1) + 1000

    return sros_filter_id


def virtual_router(root: ncs.maagic.Root, device: ncs.maagic.Container) -> bool:
    """Attempts to read the 'virtual-router' leaf of the device

    We set the virtual-router leaf in the TeraStream (CI) environment to tweak
    behavior for virtual vs. real routers. In the past this was set on
    /devices/device/virtual-router. With the transition to device automaton its
    place is not /devices/automaton/virtual-router. Let's check everywhere!"""

    try:
        return root.ncs__devices.devaut__automaton[device.name].virtual_router
    except (KeyError, AttributeError):
        pass

    try:
        return device.virtual_router
    except AttributeError:
        pass

    raise AttributeError(f'virtual-router leaf not found for device {device.name}')


class AccessListYanglateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        # we are in /devices/device{x}/translate/acls/acl{Y}
        device = service._parent._parent._parent._parent
        self.log.info(f"{service._path}: service create()")

        if 'http://tail-f.com/ned/alu-sr' in device.capability \
           or 'urn:nokia.com:sros:ns:yang:sr:conf' in device.capability:
            if not service.acl_id:
                raise ValueError(f'{service._path}: acl-id is mandatory for ALU SROS')

        entries = [AccessListEntry.from_cdb(Type.from_yang(service.type), ace) for ace in service.aces.ace]
        address_containers = {ac.address_container_name: AddressContainer.from_cdb(ac) for ac in service._parent._parent.address_container}

        filters: List[Callable[[Iterable[AccessListEntry], Dict[str, AddressContainer]], Iterable[AccessListEntry]]] = []
        if ('http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-acl-cfg' in device.capability \
           or 'http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-acl-cfg' in device.capability) \
           and virtual_router(root, device):
            # Cisco XR requires actual hardware to support object-groups. with this filter we can
            # fake object-group support on XRv (virtual) by basically creating N access list entries
            # for an object group with N network objects :)
            filters.append(expand_address_containers)
        filters.append(add_entry_id)
        self.log.debug(f'{service._path}: Using filters: {[f.__name__ for f in filters]}')

        filtered_entries: Iterable[AccessListEntry] = entries
        for f in filters:
            filtered_entries = f(filtered_entries, address_containers)
        filtered_entries = list(filtered_entries)

        generators = [generate_template_vars_acl]

        for g in generators:
            for template_name, tvars in g(filtered_entries):
                template = ncs.template.Template(service)
                template.apply(template_name, tvars)
                self.log.debug(f'{service._path}: Applying template {template_name} with variables {tvars}')
        return proplist

    @Service.pre_modification
    def cb_pre_modification(self, tctx, op, kp, root, proplist):
        if op == ncs.dp.NCS_SERVICE_DELETE:
            return proplist

        self.log.debug(f'{kp}: service pre-modification() {op}')

        # keypath is /devices/device{x}/translate/acls/acl{Y}, meaning the device name has index 4
        acl_container = root.devices.device[str(kp[4][0])].ytc__translate.acl__acls
        service = acl_container.acl[str(kp[0][0])]

        if service.acl_id:
            # check for duplicate acl-id for ACLs of the same type
            for other_service in service._parent:
                if other_service.type != service.type or other_service.name == service.name:
                    continue
                if other_service.acl_id == service.acl_id:
                    raise ValueError(f'{service._path}: acl-id {service.acl_id} already in use by {other_service._path}')
        return proplist


class AddressContainerYanglateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        # we are in /devices/device{x}/translate/acls/address-container{Y}
        device = service._parent._parent._parent._parent
        self.log.info(f"{service._path}: service create()")

        address_container = {service.address_container_name: AddressContainer.from_cdb(service)}

        generators = []
        if ('http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-acl-cfg' in device.capability \
           or 'http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-acl-cfg' in device.capability) \
           and virtual_router(root, device):
            # no object-group support on XRv, we're not doing anything here ...
            pass
        else:
            generators.append(generate_template_vars_address_container)

        for g in generators:
            for template_name, tvars in g(address_container):
                template = ncs.template.Template(service)
                template.apply(template_name, tvars)
                self.log.debug(f'{service._path}: Applying template {template_name} with variables {tvars}')


class AttachmentPointInterfaceYanglateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        # we are in /devices/device{x}/translate/acls/attachment-points/interface{Y}
        device = service._parent._parent._parent._parent._parent
        self.log.info(f"{service._path}: service create()")

        if 'http://tail-f.com/ned/alu-sr' in device.capability \
           or 'urn:nokia.com:sros:ns:yang:sr:conf' in device.capability:
            errors = []
            types_ingress = Counter(self.acl_type_from_acl_set(device, x) for x in service.ingress.acl_sets.acl_set)
            if len(service.ingress.acl_sets.acl_set) > 1 and \
                    types_ingress[Type.ipv4] > 1 and types_ingress[Type.ipv6] > 1:
                errors.append('At most 1 ingress ACL supported on SROS.')
            types_egress = Counter(self.acl_type_from_acl_set(device, x) for x in service.egress.acl_sets.acl_set)
            if len(service.ingress.acl_sets.acl_set) > 1 and \
                    types_egress[Type.ipv4] > 1 and types_ingress[Type.ipv6] > 1:
                errors.append('At most 1 egress ACL supported on SROS.')

            if errors:
                raise ValueError('\n'.join(errors))

        # for Juniper split out the physical interface and logical unit
        parts = service.interface_id.rsplit(sep='.', maxsplit=1)
        tvars = ncs.template.Variables()
        tvars.add('INTERFACE', parts[0])
        tvars.add('UNIT', parts[1] if len(parts) == 2 else '0')
        template = ncs.template.Template(service)
        template.apply(TemplateName('acl-yanglate-attachment-point-interface'), tvars)

    @staticmethod
    def acl_type_from_acl_set(device, acl_set):
        """Dereference the /attachment-point/interface/ingress/acl-sets/acl-set/name leafref & return ACL type"""
        return Type.from_yang(device.ytc__translate.acl__acls.acl[acl_set.name].type)


class ServiceApp(ncs.application.Application):
    def setup(self):
        self.register_service('acl-yanglate-access-list-servicepoint', AccessListYanglateService)
        self.register_service('acl-yanglate-address-container-servicepoint', AddressContainerYanglateService)
        self.register_service('acl-yanglate-attachment-point-interface-servicepoint', AttachmentPointInterfaceYanglateService)
