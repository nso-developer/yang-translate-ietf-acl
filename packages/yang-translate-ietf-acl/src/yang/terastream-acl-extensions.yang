module terastream-acl-extensions {

  yang-version 1.1;

  namespace "urn:telekom:terastream:acl-extensions";

  prefix ts-acl;

  import ietf-access-control-list {
    prefix acl;
  }

  import ietf-inet-types {
    prefix inet;
  }

  import tailf-common {
    prefix tailf;
  }

  import tailf-ncs {
    prefix ncs;
  }

  import yang-translate-container {
    prefix ytc;
  }

  organization
    "Terastream";

  contact
    "Kristian Larsson
    kll@dev.terastrm.net
    
    Marko Zagožen
    mzagozen@dev.terastrm.net";
  description
    "This YANG module augments IETF ACL Yang with additional structures:
    - a list for creating network groups
    - a way of refering to network groups from ACEs";

  revision 2020-05-27 {
    description
      "Add SROS specific service instance configuration";
  }

  revision 2017-08-09 {
    description
      "Initial revision";
  }

  augment "/ncs:devices/ncs:device/ytc:translate/acl:acls" {
    list address-container {
      key "address-container-type address-container-name";
      description
        "An address container allows us to group multiple IPv4/IPv6
        prefixes into a single unit. This unit can then be used in
        access list entries.";
      leaf address-container-type {
        type acl:acl-type;
      }
      leaf address-container-name {
        type string;
      }


      uses ncs:service-data;
      ncs:servicepoint acl-yanglate-address-container-servicepoint;

      list entry {
        key entry-name;
        ordered-by user;
        leaf entry-name {
          type string;
        }

        choice prefix-family {
          leaf ipv4-prefix {
            when "derived-from-or-self(../../address-container-type, 'acl:ipv4-acl-type')";
            type inet:ipv4-prefix;
          }

          leaf ipv6-prefix {
            when "derived-from-or-self(../../address-container-type, 'acl:ipv6-acl-type')";
            type inet:ipv6-prefix;
          }
        }
      }
    }
  }

  augment "/ncs:devices/ncs:device/ytc:translate/acl:acls/acl:acl/acl:aces/"
    + "acl:ace/acl:matches/acl:l3/acl:ipv6/acl:ipv6/acl:destination-network" {
    case destination-ipv6-address-container {
      leaf destination-ipv6-address-container {
        type leafref {
          path "../../../../../../ts-acl:address-container"
            + "[ts-acl:address-container-type=current()/../../../../../acl:type]/"
            + "ts-acl:address-container-name";
        }
      }
    }
  }

  augment "/ncs:devices/ncs:device/ytc:translate/acl:acls/acl:acl/acl:aces/"
    + "acl:ace/acl:matches/acl:l3/acl:ipv6/acl:ipv6/acl:source-network" {
    case source-ipv6-address-container {
      leaf source-ipv6-address-container {
        type leafref {
          path "../../../../../../ts-acl:address-container"
            + "[ts-acl:address-container-type=current()/../../../../../acl:type]/"
            + "ts-acl:address-container-name";
        }
      }
    }
  }

  augment "/ncs:devices/ncs:device/ytc:translate/acl:acls/acl:acl/acl:aces/"
    + "acl:ace/acl:matches/acl:l3/acl:ipv4/acl:ipv4/acl:destination-network" {
    case destination-ipv4-address-container {
      leaf destination-ipv4-address-container {
        type leafref {
          path "../../../../../../ts-acl:address-container"
            + "[ts-acl:address-container-type=current()/../../../../../acl:type]/"
            + "ts-acl:address-container-name";
        }
      }
    }
  }

  augment "/ncs:devices/ncs:device/ytc:translate/acl:acls/acl:acl/acl:aces/"
    + "acl:ace/acl:matches/acl:l3/acl:ipv4/acl:ipv4/acl:source-network" {
    case source-ipv4-address-container {
      leaf source-ipv4-address-container {
        type leafref {
          path "../../../../../../ts-acl:address-container"
            + "[ts-acl:address-container-type=current()/../../../../../acl:type]/"
            + "ts-acl:address-container-name";
        }
      }
    }
  }

  augment "/ncs:devices/ncs:device/ytc:translate/acl:acls/acl:acl" {
    leaf acl-id {
      type uint32;
    }
  }

  augment "/ncs:devices/ncs:device/ytc:translate/acl:acls/acl:attachment-points/acl:interface" {
    container sros {
      choice instance {
        default router;
        case router {
          leaf router {
            type string;
            default "Base";
          }
        }
        case service-ies {
          container ies {
            presence "Apply the ACL under a service ies instance";
            leaf service-name {
              type string;
              mandatory true;
            }
            leaf sap-id {
              type string;
              mandatory true;
            }
          }
        }
      }
    }
  }
}
