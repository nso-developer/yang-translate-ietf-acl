<config-template xmlns="http://tail-f.com/ns/config/1.0">
    <!-- macro for Cisco UM -->
    <?macro IOS_XR_UM_ACL_RULE default_prefix_len protocol?>
    <protocol when="{$PROTOCOL_TYPE}">{$PROTOCOL_TYPE}</protocol>
    <protocol when="{not($PROTOCOL_TYPE)}">$protocol</protocol>
    <source>
        <!-- IOS XR translate
            <address>1:2::3</address>
            <prefix-length>128</prefix-length>
            to
            <host>1:2::3</host>
            then re-deploy fails because we can't set both host and address!
        -->
        <host when="{($SOURCE_PREFIX_LENGTH = $default_prefix_len) or not($SOURCE_PREFIX_LENGTH)}">{$SOURCE_NETWORK}</host>
        <address when="{not($SOURCE_PREFIX_LENGTH = $default_prefix_len) and $SOURCE_PREFIX_LENGTH}">{$SOURCE_NETWORK}</address>
        <prefix-length when="{$SOURCE_PREFIX_LENGTH and not($SOURCE_PREFIX_LENGTH = $default_prefix_len)}">{$SOURCE_PREFIX_LENGTH}</prefix-length>
        <port when="{$SOURCE_PORT_UM_NAME}">
            <eq>{$SOURCE_PORT_UM_NAME}</eq>
        </port>
        <any when="{not($SOURCE_NETWORK)}"/>
    </source>
    <destination>
        <host when="{($DESTINATION_PREFIX_LENGTH = $default_prefix_len) or not($DESTINATION_PREFIX_LENGTH)}">{$DESTINATION_NETWORK}</host>
        <address when="{not($DESTINATION_PREFIX_LENGTH = $default_prefix_len) and $DESTINATION_PREFIX_LENGTH}">{$DESTINATION_NETWORK}</address>
        <prefix-length when="{$DESTINATION_PREFIX_LENGTH and not($DESTINATION_PREFIX_LENGTH = $default_prefix_len)}">{$DESTINATION_PREFIX_LENGTH}</prefix-length>
        <port when="{$DESTINATION_PORT_UM_NAME}">
            <eq>{$DESTINATION_PORT_UM_NAME}</eq>
        </port>
        <any when="{not($DESTINATION_NETWORK)}"/>
    </destination>
    <?endmacro?>

    <devices xmlns="http://tail-f.com/ns/ncs">
        <device tags="nocreate">
            <!-- the XPath context node for this template is /devices/device{X}/translate/acls/acl{Y} -->
            <name>{../../../name}</name>
            <config tags="merge">

                <!-- ALU SR-OS -->
                <filter xmlns="http://tail-f.com/ned/alu-sr">

                    <!-- ipv4 type -->
                    <ip-filter when="{/type='acl:ipv4-acl-type'}">
                        <ip-filter-name>{/acl-id}</ip-filter-name>
                        <name>{/acl-id}</name>
                        <entry>
                            <entry-name>{$ENTRY_ID}</entry-name>
                            <description>{$NAME}</description>
                            <?if {$CONTAINS_MATCH='true'}?>
                            <match>
                                <!-- The protocol-type leaf is the key for the match list. If no value is given,
                                     it defaults to empty string. If a value is provided we must also create the
                                     empty protocol leaf.
                                     The use of the string() XPath function here is deliberate. A plain XPath variable
                                     with an empty string value instructs NSO templating engine to skip the node! -->
                                <protocol-type>{string($PROTOCOL_TYPE)}</protocol-type>
                                <?if {$PROTOCOL_TYPE!=''}?>
                                <protocol/>
                                <?end?>
                                <src-ip when="{$SOURCE_PREFIX or $SOURCE_ADDRESS_CONTAINER}">
                                    <address when="{$SOURCE_PREFIX}">{$SOURCE_PREFIX}</address>
                                    <ip-prefix-list>{$SOURCE_ADDRESS_CONTAINER}</ip-prefix-list>
                                </src-ip>
                                <dst-ip when="{$DESTINATION_PREFIX or $DESTINATION_ADDRESS_CONTAINER}">
                                    <address when="{$DESTINATION_PREFIX}">{$DESTINATION_PREFIX}</address>
                                    <ip-prefix-list>{$DESTINATION_ADDRESS_CONTAINER}</ip-prefix-list>
                                </dst-ip>
                                <src-port when="{$SOURCE_PORT}">
                                    <op>eq</op>
                                    <port>{$SOURCE_PORT}</port>
                                </src-port>
                                <dst-port when="{$DESTINATION_PORT}">
                                    <op>eq</op>
                                    <port>{$DESTINATION_PORT}</port>
                                </dst-port>
                            </match>
                            <?end?>
                            <action>
                                <!-- in NSO 4.6, can replace this with a conditional :) -->
                                <drop when="{$ACTION='reject' or $ACTION='drop'}" />
                                <forward when="{$ACTION='accept'}" />
                            </action>
                        </entry>
                    </ip-filter>

                    <!-- ipv6 type -->
                    <ipv6-filter when="{/type='acl:ipv6-acl-type'}">
                        <ip-filter-name>{/acl-id}</ip-filter-name>
                        <name>{/acl-id}</name>
                        <entry>
                            <entry-name>{$ENTRY_ID}</entry-name>
                            <description>{$NAME}</description>
                            <match>
                                <next-header when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_NUMBER}</next-header>
                                <src-ip when="{$SOURCE_PREFIX or $SOURCE_ADDRESS_CONTAINER}">
                                    <address when="{$SOURCE_PREFIX}">{$SOURCE_PREFIX}</address>
                                    <ipv6-prefix-list>{$SOURCE_ADDRESS_CONTAINER}</ipv6-prefix-list>
                                </src-ip>
                                <dst-ip when="{$DESTINATION_PREFIX or $DESTINATION_ADDRESS_CONTAINER}">
                                    <address when="{$DESTINATION_PREFIX}">{$DESTINATION_PREFIX}</address>
                                    <ipv6-prefix-list>{$DESTINATION_ADDRESS_CONTAINER}</ipv6-prefix-list>
                                </dst-ip>
                                <src-port when="{$SOURCE_PORT}">
                                    <op>eq</op>
                                    <port>{$SOURCE_PORT}</port>
                                </src-port>
                                <dst-port when="{$DESTINATION_PORT}">
                                    <op>eq</op>
                                    <port>{$DESTINATION_PORT}</port>
                                </dst-port>
                            </match>
                            <action-mode>
                                <!-- in NSO 4.6, can replace this with a conditional :) -->
                                <action when="{$ACTION='reject' or $ACTION='drop'}">drop</action>
                                <action when="{$ACTION='accept'}">forward</action>
                            </action-mode>
                        </entry>
                    </ipv6-filter>
                </filter>

                <!-- NETCONF SR-OS -->
                <configure xmlns="urn:nokia.com:sros:ns:yang:sr:conf">
                    <filter>
                        <ip-filter when="{/type='acl:ipv4-acl-type'}">
                            <filter-name>{/acl-id}</filter-name>
                            <entry>
                                <entry-id>{$ENTRY_ID}</entry-id>
                                <description>{$NAME}</description>
                                <match>
                                    <protocol when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_NUMBER}</protocol>
                                    <src-ip when="{$SOURCE_PREFIX or $SOURCE_ADDRESS_CONTAINER}">
                                        <address when="{$SOURCE_PREFIX}">{$SOURCE_PREFIX}</address>
                                        <ip-prefix-list>{$SOURCE_ADDRESS_CONTAINER}</ip-prefix-list>
                                    </src-ip>
                                    <dst-ip when="{$DESTINATION_PREFIX or $DESTINATION_ADDRESS_CONTAINER}">
                                        <address when="{$DESTINATION_PREFIX}">{$DESTINATION_PREFIX}</address>
                                        <ip-prefix-list>{$DESTINATION_ADDRESS_CONTAINER}</ip-prefix-list>
                                    </dst-ip>
                                </match>
                                <action>
                                    <accept when="{$ACTION='accept'}" />
                                    <drop when="{$ACTION='reject' or $ACTION='drop'}" />
                                </action>
                            </entry>
                        </ip-filter>
                        <ipv6-filter when="{/type='acl:ipv6-acl-type'}">
                            <filter-name>{/acl-id}</filter-name>
                            <entry>
                                <entry-id>{$ENTRY_ID}</entry-id>
                                <description>{$NAME}</description>
                                <match>
                                    <next-header when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_NUMBER}</next-header>
                                    <src-ip when="{$SOURCE_PREFIX or $SOURCE_ADDRESS_CONTAINER}">
                                        <address when="{$SOURCE_PREFIX}">{$SOURCE_PREFIX}</address>
                                        <ipv6-prefix-list>{$SOURCE_ADDRESS_CONTAINER}</ipv6-prefix-list>
                                    </src-ip>
                                    <dst-ip when="{$DESTINATION_PREFIX or $DESTINATION_ADDRESS_CONTAINER}">
                                        <address when="{$DESTINATION_PREFIX}">{$DESTINATION_PREFIX}</address>
                                        <ipv6-prefix-list>{$DESTINATION_ADDRESS_CONTAINER}</ipv6-prefix-list>
                                    </dst-ip>
                                </match>
                                <action>
                                    <accept when="{$ACTION='accept'}" />
                                    <drop when="{$ACTION='reject' or $ACTION='drop'}" />
                                </action>
                            </entry>
                        </ipv6-filter>
                    </filter>
                </configure>

                <!-- Juniper Junos -->
                <configuration xmlns="http://xml.juniper.net/xnm/1.1/xnm">
                    <firewall>
                        <family>
                            <!-- ipv4 type -->
                            <inet when="{/type='acl:ipv4-acl-type'}">
                                <filter>
                                    <name>{/name}</name>
                                    <term>
                                        <name>{$NAME}</name>
                                        <from>
                                            <protocol when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_NUMBER}</protocol>
                                            <source-address when="{$SOURCE_PREFIX}">
                                                <name>{$SOURCE_PREFIX}</name>
                                            </source-address>
                                            <source-prefix-list when="{$SOURCE_ADDRESS_CONTAINER}">
                                                <name>{$SOURCE_ADDRESS_CONTAINER}</name>
                                            </source-prefix-list>
                                            <destination-address when="{$DESTINATION_PREFIX}">
                                                <name>{$DESTINATION_PREFIX}</name>
                                            </destination-address>
                                            <destination-prefix-list when="{$DESTINATION_ADDRESS_CONTAINER}">
                                                <name>{$DESTINATION_ADDRESS_CONTAINER}</name>
                                            </destination-prefix-list>
                                            <source-port when="{$SOURCE_PORT}">{$SOURCE_PORT}</source-port>
                                            <destination-port when="{$DESTINATION_PORT}">{$DESTINATION_PORT}</destination-port>
                                        </from>
                                        <then>
                                            <accept when="{$ACTION='accept'}" />
                                            <discard when="{$ACTION='drop'}" />
                                            <reject when="{$ACTION='reject'}" />
                                        </then>
                                    </term>
                                </filter>
                            </inet>

                            <!-- ipv6 type -->
                            <inet6 when="{/type='acl:ipv6-acl-type'}">
                                <filter>
                                    <name>{/name}</name>
                                    <term>
                                        <name>{$NAME}</name>
                                        <from>
                                            <next-header when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_NUMBER}</next-header>
                                            <source-address when="{$SOURCE_PREFIX}">
                                                <name>{$SOURCE_PREFIX}</name>
                                            </source-address>
                                            <source-prefix-list when="{$SOURCE_ADDRESS_CONTAINER}">
                                                <name>{$SOURCE_ADDRESS_CONTAINER}</name>
                                            </source-prefix-list>
                                            <destination-address when="{$DESTINATION_PREFIX}">
                                                <name>{$DESTINATION_PREFIX}</name>
                                            </destination-address>
                                            <destination-prefix-list when="{$DESTINATION_ADDRESS_CONTAINER}">
                                                <name>{$DESTINATION_ADDRESS_CONTAINER}</name>
                                            </destination-prefix-list>
                                            <source-port when="{$SOURCE_PORT}">{$SOURCE_PORT}</source-port>
                                            <destination-port when="{$DESTINATION_PORT}">{$DESTINATION_PORT}</destination-port>
                                        </from>
                                        <then>
                                            <accept when="{$ACTION='accept'}" />
                                            <discard when="{$ACTION='drop'}" />
                                            <reject when="{$ACTION='reject'}" />
                                        </then>
                                    </term>
                                </filter>
                            </inet6>
                        </family>
                    </firewall>
                </configuration>

                <!-- Juniper Junos Native -->
                <configuration xmlns="http://yang.juniper.net/junos/conf/root">
                    <firewall xmlns="http://yang.juniper.net/junos/conf/firewall">
                        <family>
                            <!-- ipv4 type -->
                            <inet when="{/type='acl:ipv4-acl-type'}">
                                <filter>
                                    <name>{/name}</name>
                                    <term>
                                        <name>{$NAME}</name>
                                        <from>
                                            <protocol when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_NUMBER}</protocol>
                                            <source-address when="{$SOURCE_PREFIX}">
                                                <name>{$SOURCE_PREFIX}</name>
                                            </source-address>
                                            <source-prefix-list when="{$SOURCE_ADDRESS_CONTAINER}">
                                                <name>{$SOURCE_ADDRESS_CONTAINER}</name>
                                            </source-prefix-list>
                                            <destination-address when="{$DESTINATION_PREFIX}">
                                                <name>{$DESTINATION_PREFIX}</name>
                                            </destination-address>
                                            <destination-prefix-list when="{$DESTINATION_ADDRESS_CONTAINER}">
                                                <name>{$DESTINATION_ADDRESS_CONTAINER}</name>
                                            </destination-prefix-list>
                                            <source-port when="{$SOURCE_PORT}">{$SOURCE_PORT}</source-port>
                                            <destination-port when="{$DESTINATION_PORT}">{$DESTINATION_PORT}</destination-port>
                                        </from>
                                        <then>
                                            <accept when="{$ACTION='accept'}" />
                                            <discard when="{$ACTION='drop'}" />
                                            <reject when="{$ACTION='reject'}" />
                                        </then>
                                    </term>
                                </filter>
                            </inet>

                            <!-- ipv6 type -->
                            <inet6 when="{/type='acl:ipv6-acl-type'}">
                                <filter>
                                    <name>{/name}</name>
                                    <term>
                                        <name>{$NAME}</name>
                                        <from>
                                            <next-header when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_NUMBER}</next-header>
                                            <source-address when="{$SOURCE_PREFIX}">
                                                <name>{$SOURCE_PREFIX}</name>
                                            </source-address>
                                            <source-prefix-list when="{$SOURCE_ADDRESS_CONTAINER}">
                                                <name>{$SOURCE_ADDRESS_CONTAINER}</name>
                                            </source-prefix-list>
                                            <destination-address when="{$DESTINATION_PREFIX}">
                                                <name>{$DESTINATION_PREFIX}</name>
                                            </destination-address>
                                            <destination-prefix-list when="{$DESTINATION_ADDRESS_CONTAINER}">
                                                <name>{$DESTINATION_ADDRESS_CONTAINER}</name>
                                            </destination-prefix-list>
                                            <source-port when="{$SOURCE_PORT}">{$SOURCE_PORT}</source-port>
                                            <destination-port when="{$DESTINATION_PORT}">{$DESTINATION_PORT}</destination-port>
                                        </from>
                                        <then>
                                            <accept when="{$ACTION='accept'}" />
                                            <discard when="{$ACTION='drop'}" />
                                            <reject when="{$ACTION='reject'}" />
                                        </then>
                                    </term>
                                </filter>
                            </inet6>
                        </family>
                    </firewall>
                </configuration>
                <!-- Cisco XR -->
                <!-- ipv4 type -->
                <ipv4-acl-and-prefix-list xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-acl-cfg" when="{/type='acl:ipv4-acl-type'}">
                    <accesses>
                        <access>
                            <access-list-name>{/name}</access-list-name>
                            <access-list-entries>
                                <access-list-entry>
                                    <sequence-number>{$ENTRY_ID}</sequence-number>
                                    <grant when="{$ACTION='accept'}">permit</grant>
                                    <grant when="{$ACTION='drop' or $ACTION='reject'}">deny</grant>
                                    <!-- protocol-operator=equal will always appear in configuration ... -->
                                    <protocol-operator>equal</protocol-operator>
                                    <protocol when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_TYPE}</protocol>
                                    <source-network when="{$SOURCE_PREFIX}">
                                        <source-address>{$SOURCE_NETWORK}</source-address>
                                        <source-prefix-length>{$SOURCE_PREFIX_LENGTH}</source-prefix-length>
                                    </source-network>
                                    <source-prefix-group>{$SOURCE_ADDRESS_CONTAINER}</source-prefix-group>
                                    <source-port when="{$SOURCE_PORT}">
                                        <source-operator>equal</source-operator>
                                        <first-source-port>{$SOURCE_PORT}</first-source-port>
                                    </source-port>
                                    <destination-network when="{$DESTINATION_PREFIX}">
                                        <destination-address>{$DESTINATION_NETWORK}</destination-address>
                                        <destination-prefix-length>{$DESTINATION_PREFIX_LENGTH}</destination-prefix-length>
                                    </destination-network>
                                    <destination-prefix-group>{$DESTINATION_ADDRESS_CONTAINER}</destination-prefix-group>
                                    <destination-port when="{$DESTINATION_PORT}">
                                        <destination-operator>equal</destination-operator>
                                        <first-destination-port>{$DESTINATION_PORT}</first-destination-port>
                                    </destination-port>
                                    <sequence-str>{$ENTRY_ID}</sequence-str>
                                </access-list-entry>
                            </access-list-entries>
                        </access>
                    </accesses>
                </ipv4-acl-and-prefix-list>

                <!-- ipv6 type -->
                <ipv6-acl-and-prefix-list xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-acl-cfg" when="{/type='acl:ipv6-acl-type'}">
                    <accesses>
                        <access>
                            <name>{/name}</name>
                            <access-list-entries>
                                <access-list-entry>
                                    <sequence-number>{$ENTRY_ID}</sequence-number>
                                    <grant when="{$ACTION='accept'}">permit</grant>
                                    <grant when="{$ACTION='drop' or $ACTION='reject'}">deny</grant>
                                    <!-- protocol-operator=equal will always appear in configuration ... -->
                                    <protocol-operator>equal</protocol-operator>
                                    <protocol when="{$PROTOCOL_NUMBER!='-1'}">{$PROTOCOL_TYPE}</protocol>
                                    <source-network when="{$SOURCE_PREFIX}">
                                        <source-address>{$SOURCE_NETWORK}</source-address>
                                        <!-- this will appear on the device as network/prefixlen (2003:1800::/22), even though it says wildcard ?! -->
                                        <source-wild-card-bits>{$SOURCE_WILDCARD_PREFIX_LENGTH}</source-wild-card-bits>
                                    </source-network>
                                    <source-prefix-group>{$SOURCE_ADDRESS_CONTAINER}</source-prefix-group>
                                    <source-port when="{$SOURCE_PORT_NAME}">
                                        <source-operator>equal</source-operator>
                                        <first-source-port>{$SOURCE_PORT_NAME}</first-source-port>
                                    </source-port>
                                    <destination-network when="{$DESTINATION_PREFIX}">
                                        <destination-address>{$DESTINATION_NETWORK}</destination-address>
                                        <!-- this will appear on the device as network/prefixlen (2003:1800::/22), even though it says wildcard ?! -->
                                        <destination-wild-card-bits>{$DESTINATION_WILDCARD_PREFIX_LENGTH}</destination-wild-card-bits>
                                    </destination-network>
                                    <destination-prefix-group>{$DESTINATION_ADDRESS_CONTAINER}</destination-prefix-group>
                                    <destination-port when="{$DESTINATION_PORT_NAME}">
                                        <destination-operator>equal</destination-operator>
                                        <first-destination-port>{$DESTINATION_PORT_NAME}</first-destination-port>
                                    </destination-port>
                                    <sequence-str>{$ENTRY_ID}</sequence-str>
                                </access-list-entry>
                            </access-list-entries>
                        </access>
                    </accesses>
                </ipv6-acl-and-prefix-list>
                <!-- Cisco XR UM -->
                <ipv4 xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-um-ipv4-access-list-cfg" when="{/type='acl:ipv4-acl-type'}">
                    <access-lists>
                        <access-list>
                            <access-list-name>{/name}</access-list-name>
                            <sequences>
                                <sequence>
                                    <sequence-number>{$ENTRY_ID}</sequence-number>
                                    <?if {$ACTION = 'accept'} ?>
                                    <permit>
                                        <?expand IOS_XR_UM_ACL_RULE default_prefix_len=32 protocol='ipv4'?>
                                    </permit>
                                    <?end?>
                                    <?if {$ACTION='drop' or $ACTION='reject'} ?>
                                    <deny>
                                        <?expand IOS_XR_UM_ACL_RULE default_prefix_len=32 protocol='ipv4'?>
                                    </deny>
                                    <?end?>
                                </sequence>
                            </sequences>
                        </access-list>
                    </access-lists>
                </ipv4>
                <ipv6 xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-um-ipv6-access-list-cfg"  when="{/type='acl:ipv6-acl-type'}">
                    <access-lists>
                        <access-list>
                            <access-list-name>{/name}</access-list-name>
                            <sequences>
                                <sequence>
                                    <sequence-number>{$ENTRY_ID}</sequence-number>
                                    <?if {$ACTION = 'accept'} ?>
                                    <permit>
                                        <?expand IOS_XR_UM_ACL_RULE default_prefix_len=128 protocol='ipv6'?>
                                    </permit>
                                    <?end?>
                                    <?if {$ACTION='drop' or $ACTION='reject'} ?>
                                    <deny>
                                        <?expand IOS_XR_UM_ACL_RULE default_prefix_len=128 protocol='ipv6'?>
                                    </deny>
                                    <?end?>
                                </sequence>
                            </sequences>
                        </access-list>
                    </access-lists>
                </ipv6>
            </config>
        </device>
    </devices>
</config-template>
